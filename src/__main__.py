import os
import sys
import logging
from pathlib import Path
from src.Generator import Generator

# delete/recreate log files
filepath = Path(__file__)
if (filepath.parent.parent / 'logs/generator.log').exists():
    (filepath.parent.parent / 'logs/generator.log').unlink()
Path(filepath.parent.parent , 'logs').mkdir(exist_ok=True)

# configure logging
logging.basicConfig(level=logging.DEBUG, stream=sys.stdout, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

logger = logging.getLogger('Generator')
logger.propagate = True
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# fh = logging.FileHandler('logs/generator.log')
# fh.setLevel(logging.INFO)
# fh.setFormatter(formatter)
# logger.addHandler(fh)

if __name__ == "__main__":
    """Entry point for application running with : python -m <modulename>
    """
    logger.info('Starting Generator')

    # get env vars
    logger.info(f'Getting environement variables')
    input_dir = os.getenv('SENTINEL_DIR')
    output_dir = os.getenv('OUTPUT', '/home/eouser/floodrider')

    if not Path(input_dir).exists():
        logger.info(f'Sentinel directory: {input_dir} does not exist')
        sys.exit()

    if not Path(output_dir).exists():
        Path(output_dir).mkdir(parents=True)

    # generate 
    try:
        g = Generator(input_dir, output_dir, logger)
        if g.run():
            logger.info('Run completed successfully')
        else:
            logger.error('Run completed with errors')

    except KeyboardInterrupt:
        logger.info('User Exit')
        logging.shutdown()
        sys.exit()

    except Exception as e:
        logger.exception(f'Exception occurred during generation: {str(e)}')
        logging.shutdown()
        sys.exit()
